
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rahul
 */
public class HMMTagger {

    File test;
    File transitions;
    File emissions;

    Training training;
    BaselineTagger tagger;

    ArrayList<ArrayList<Double>> matrixA;
    ArrayList<ArrayList<Double>> matrixB;

    public HMMTagger(String initTest, Training initTraining, ArrayList<ArrayList<Double>> initTransitions, ArrayList<ArrayList<Double>> initEmissions) throws IOException {
        test = new File(initTest);
        tagger = new BaselineTagger("training.txt", "test.txt");

        training = new Training(initTest);

        matrixA = initTransitions;
        matrixB = initEmissions;

    }

    public String[] Viterbi(Sentence text) {
        ArrayList<String> tags = training.getTagList();
        double[][] scores = new double[text.getWords().size()][tags.size()];
        String[][] backPointers = new String[text.getWords().size()][tags.size()];

        for (int i = 0; i < tags.size() && i < matrixA.get(0).size(); i++) {
            scores[1][i] = matrixA.get(0).get(i);
            backPointers[1][i] = null;
        }

        for (int i = 1; i < text.getWords().size() && i < scores.length; i++) {
            for (int j = 1; j < tags.size() && j < matrixB.size(); j++) {
                double max = 0;
                String tag = "";
                for (int k = 0; k < tags.size() && k < matrixA.size(); k++) {
                    if (i < scores.length - 1 && k < scores[i - 1].length && k < matrixA.size() && j < matrixA.get(k).size() && j < matrixB.size() && i < matrixB.get(j).size()) {
                        double value = scores[i - 1][k] * matrixA.get(k).get(j) * matrixB.get(j).get(i);
                        if (value > max) {
                            max = value;
                            tag = tags.get(k);
                        }
                    }
                }
                scores[i][j] = max;
                backPointers[i][j] = tag;
            }
        }

        String[] bestTags = new String[text.getWords().size()];
        double max = 0;
        String bestTag = "";
        for (int k = 0; k < tags.size(); k++) {
            if(k<scores[scores.length-1].length) {
            if (scores[scores.length-1][k] > max) {
                max = scores[tags.size()][k];
                bestTag = backPointers[tags.size()][k];
            }
            }
        }

        bestTags[scores.length-1] = bestTag;

        for (int k = bestTags.length - 2; k >= 1; k--) {
            bestTags[k] = backPointers[k + 1][k + 1];
        }

        return bestTags;
    }

    public ArrayList<String[]> ConvertSentences() {
        ArrayList<String[]> bestTags = new ArrayList<String[]>();
        ArrayList<Sentence> sentences = tagger.testData;
        for (Sentence sentence : sentences) {
            String[] tags = Viterbi(sentence);
            bestTags.add(tags);
        }

        return bestTags;
    }

    public void printAllTags(String choice) throws IOException {
        //ArrayList<String[]> tags = ConvertSentences();
        
        TestProbabilities generatedHMM = new TestProbabilities(("Test-Generated.txt"));
        generatedHMM.getEmissions();
        
        if(choice.equals("MLE")) {
            for(int i=0; i<generatedHMM.mle.size(); i++) {
                MLEEmissionData element = generatedHMM.mle.get(i);
                if(element.mlePossibility == 0.0 && element.getWord().equals("<s>") && element.getTag().equals("<s>")) System.out.println();
                else System.out.print("[" + element.getTag() + ", " + element.mlePossibility+"]");
            }
        }
        else if(choice.equals("Laplace")) {
            for(int i=0; i<generatedHMM.laplace.size(); i++) {
                LaplaceEmissionData element = generatedHMM.laplace.get(i);
                if(element.laplacePossibility == 0.0 && element.getWord().equals("<s>") && element.getTag().equals("<s>")) System.out.println();
                else System.out.print("[" + element.getTag() + ", " + element.laplacePossibility+"]");
            }
        }
        else System.out.println("INVALID INPUT");
    }
}
