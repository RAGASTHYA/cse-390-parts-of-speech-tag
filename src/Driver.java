
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rahul
 */
public class Driver {
    
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        
        System.out.println("Enter the training data file name: (please append .txt)");
        String trainingName = in.nextLine();
        System.out.println("Enter the test data file name: (please append .txt)");
        String testName = in.nextLine();
        
        Training training = new Training("training.txt");
        training.writeFiles();

        System.out.println("MATRICES:\n");
        System.out.println("MATRIX A:");
        ArrayList<ArrayList<Double>> matrixA = training.TransitionMatrix();
        for(ArrayList<Double> list: matrixA) {
            for(double element: list) {
                System.out.printf("%-30s",element);
                System.out.print("|");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("MATRIX B:");
        ArrayList<ArrayList<Double>> matrixB = training.TransitionMatrix();
        for(ArrayList<Double> list: matrixB) {
            for(double element: list) {
                System.out.printf("%-30s",element);
                System.out.print("|");
            }
            System.out.println();
        }
        
        BaselineTagger tagger = new BaselineTagger(trainingName, testName);
        tagger.makeSequence();     
        
        HMMTagger hmm = new HMMTagger(testName, training, matrixA, matrixB);
        System.out.println("1. MLE \n2. LAPLACE \nChoose One");
        String choice = in.nextLine();
        if(choice.charAt(0) == '1')
            hmm.printAllTags("MLE");
        else if(choice.charAt(0) == '2')
            hmm.printAllTags("Laplace");
        else System.out.println("INVALID INPUT");
        
        Evaluator eval = new Evaluator(tagger, tagger.tagSequence);
        
        System.out.println("Acurcy : " + eval.getAccurary());
        System.out.println();
        eval.printPrecision();
        System.out.println();
        eval.printRecall();
        System.out.println();
        eval.printF1();
    }
}
