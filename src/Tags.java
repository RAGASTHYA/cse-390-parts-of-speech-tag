/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rahul
 */
public class Tags {
    String tag;
    double possibility;

    public Tags(String tag, double possibility) {
        this.tag = tag;
        this.possibility = possibility;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public double getPossibility() {
        return possibility;
    }

    public void setPossibility(double possibility) {
        this.possibility = possibility;
    }
    
    public String toString() {
        return String.format("%-20s%-20s", tag, possibility);
    }
}
