/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rahul
 */
public class LaplaceTransitionData {
    double laplacePossibility;
    
    String tag1;
    String tag2;

    public LaplaceTransitionData(double laplacePossibility, String tag1, String tag2) {
        this.laplacePossibility = laplacePossibility;
        this.tag1 = tag1;
        this.tag2 = tag2;
    }
    
    public double getLaplacePossibility() {
        return laplacePossibility;
    }

    public void setLaplacePossibility(double laplacepossibility) {
        this.laplacePossibility = laplacepossibility;
    }

    public String getTag1() {
        return tag1;
    }

    public void setTag1(String tag1) {
        this.tag1 = tag1;
    }

    public String getTag2() {
        return tag2;
    }

    public void setTag2(String tag2) {
        this.tag2 = tag2;
    }
    
    public String toString() {
        return String.format("%-20s%-20s%-20s", tag1, tag2, laplacePossibility);
    }
}
