
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rahul
 */
public class BaselineTagger {

    ArrayList<Sentence> testData;
    TextData test;
    Training training;
    FileWriter generated;

    Hashtable<Integer, WordTagPair> tagSequence;

    public BaselineTagger(String trainingFile, String testFile) throws IOException {
        testData = new ArrayList<Sentence>();
        training = new Training(trainingFile);
        test = new TextData();
        test.readTest(testFile);
        testData = test.getTestData();

        tagSequence = new Hashtable<Integer, WordTagPair>();
    }

    public int countTags(String tag) {
        int count = 0;
        for(Sentence sen: testData) {
            for(Word word: sen.getWords()) {
                if(word.getTag().trim().equals(tag)) count++;
            }
        }
        return count;
    }
    
    public void makeSequence() throws IOException {
        training.getTransitions();
        training.getEmissions();
        ArrayList<String> words = training.getWordList();
        Hashtable<Double, LaplaceEmissionData> laplaceEmissionTraining = training.getLaplaceEmission();
        generated = new FileWriter("Test-Generated.txt");
        BufferedWriter generatedFileWriter = new BufferedWriter(generated);

        for (Sentence sen : testData) {
            generatedFileWriter.write(sen.getNumber() + "\t");
            for (int i = 0; i < sen.getWords().size(); i++) {
                Word preceding = new Word();
                if (i == 0) {
                    preceding = null;
                } else {
                    preceding = sen.getWords().get(i - 1);
                }

                Word word = sen.getWords().get(i);
                boolean found = false;
                Iterator<Map.Entry<Double, LaplaceEmissionData>> itLPE = laplaceEmissionTraining.entrySet().iterator();
                double lapalceEmission = 0;
                String tag = "";
                while (itLPE.hasNext()) {
                    double laplaceCheck = 0;
                    String tagCheck = "";
                    Map.Entry<Double, LaplaceEmissionData> lpE = itLPE.next();
                    if (word.getWord().trim().equals(lpE.getValue().getWord())) {
                        laplaceCheck = lpE.getValue().getLaplacePossibility();
                        tagCheck = lpE.getValue().getTag();
                        found = true;
                    }
                    if (laplaceCheck > lapalceEmission) {
                        lapalceEmission = laplaceCheck;
                        tag = tagCheck;
                    }
                }

                if (found) {
                    WordTagPair newPair = new WordTagPair(word.getWord(), tag, false);
                    int tagCount = countTags(tag);
                    generatedFileWriter.write(newPair.getWord() + "/" + newPair.getTag() + " ");
                    if (tagSequence.get(tagCount) != null) {
                        continue;
                    } else {
                        tagSequence.put(tagCount, newPair);
                    }
                } else {
                    tag = generateTags(preceding, word.getWord().trim());
                    WordTagPair newPair = new WordTagPair(word.getWord(), tag, true);
                    int tagCount = countTags(tag);
                    if (tagSequence.get(tagCount) != null) {
                        continue;
                    } else {
                        tagSequence.put(tagCount, newPair);
                    }
                    generatedFileWriter.write(word.getWord().trim() + "/" + tag + " ");
                }
            }
            generatedFileWriter.write("\n");
        }
    }

    public String generateTags(Word preceding, String word) {
        if (!wordIsValid(word)) {
            return "-NONE-";
        }
        String tag = "";
        if (isNumber(word)) {
            tag = "CD";
        }
        else if (isInterrogativePronoun(word)) {
            tag = "WP";
        }
        else if (isArticle(word)) {
            tag = "DT";
        }
        else if (isPreposition(word)) {
            tag = "IN";
        }
        else if (isConjunction(preceding, word)) {
            tag = "CC";
        }
        else if (isCommonNoun(preceding, word)) {
            if (isPlural(word)) {
                tag = "NNS";
            } else {
                tag = "NN";
            }
        }
        else if (isProperNoun(preceding, word)) {
            if (isPlural(word)) {
                tag = "NNPS";
            } else {
                tag = "NNP";
            }
        }
        else if (isPunctuation(word)) {
            tag = word;
        }
        else if (isPronoun(preceding, word)) {
            tag = "PRP";
        }
        else if (isVerb(preceding, word)) {
            tag = "VB";
        }
        else if (isPastVerb(preceding, word)) {
            tag = "VBD";
        }
        else if (isGerund(preceding, word)) {
            tag = "VBG";
        }
        else if (isVBP(preceding, word)) {
            tag = "VBP";
        }
        else if (isVBZ(preceding, word)) {
            tag = "VBZ";
        }
        else if (isModalVerb(preceding, word)) {
            tag = "MI";
        }
        else if (isTo(word)) {
            tag = "TO";
        }
        else if (isAdjective(preceding, word)) {
            if (isComparative(word)) {
                tag = "JJR";
            }
            if (isSuperlative(word)) {
                tag = "JJS";
            } else {
                tag = "JJ";
            }
        }
        else if (isAdverb(preceding, word)) {
            if (isComparative(word)) {
                tag = "RBR";
            }
            if (isSuperlative(word)) {
                tag = "RBS";
            } else {
                tag = "RB";
            }
        }
        return tag;
    }

    public boolean isInterrogativePronoun(String word) {
        ArrayList<String> words = new ArrayList<String>();
        words.add("what");
        words.add("which");
        words.add("who");
        words.add("whose");
        words.add("whom");
        words.add("how");

        for (String element : words) {
            if (word.toLowerCase().equals(element)) {
                return true;
            }
        }
        return false;
    }

    public boolean isArticle(String word) {
        ArrayList<String> words = new ArrayList<String>();
        words.add("a");
        words.add("an");
        words.add("the");
        words.add("those");
        words.add("that");

        for (String element : words) {
            if (word.toLowerCase().equals(element)) {
                return true;
            }
        }
        return false;
    }

    public boolean isPreposition(String word) {
        ArrayList<String> words = new ArrayList<String>();

        words.add("about");
        words.add("above");
        words.add("across");
        words.add("after");
        words.add("against");
        words.add("along");
        words.add("among");
        words.add("around");
        words.add("at");
        words.add("before");
        words.add("behind");
        words.add("below");
        words.add("beneath");
        words.add("beside");
        words.add("between");
        words.add("by");
        words.add("down");
        words.add("during");
        words.add("except");
        words.add("from");
        words.add("in");
        words.add("front");
        words.add("inside");
        words.add("instead");
        words.add("into");
        words.add("like");
        words.add("near");
        words.add("of");
        words.add("on");
        words.add("onto");
        words.add("without");
        words.add("top");
        words.add("out");
        words.add("outside");
        words.add("over");
        words.add("past");
        words.add("since");
        words.add("through");
        words.add("to");
        words.add("toward");
        words.add("under");
        words.add("underneath");
        words.add("untill");
        words.add("up");
        words.add("upon");
        words.add("with");
        words.add("than");
        words.add("that");

        for (String element : words) {
            if (word.toLowerCase().equals(element)) {
                return true;
            }
        }
        return false;
    }

    public boolean isConjunction(Word preceding, String word) {
        ArrayList<String> words = new ArrayList<String>();
        ArrayList<String> additional = new ArrayList<String>();
        words.add("and");
        words.add("but");
        words.add("for");
        words.add("nor");
        words.add("or");
        words.add("so");
        words.add("yet");
        words.add("after");
        words.add("although");
        words.add("as");
        words.add("if");
        words.add("because");
        words.add("before");
        words.add("even");
        words.add("though");
        words.add("once");
        words.add("provided");
        words.add("since");
        words.add("whenever");
        words.add("while");
        words.add("accordingly");
        words.add("also");
        words.add("anyway");
        words.add("besides");
        words.add("consequently");
        words.add("finally");
        words.add("further");
        words.add("furthermore");
        words.add("hence");
        words.add("likewise");
        words.add("meanwhile");
        words.add("moreover");
        words.add("namely");
        words.add("now");
        words.add("otherwise");
        words.add("nevertheless");
        words.add("next");
        words.add("nonetheless");
        words.add("similarly");
        words.add("still");
        words.add("then");
        words.add("therefore");
        words.add("thus");

        additional.add("example");
        additional.add("instance");
        additional.add("course");
        additional.add("far");
        additional.add("now");

        for (String element : words) {
            if (word.toLowerCase().equals(element)) {
                return true;
            }
        }
        for (String element : additional) {
            if (word.toLowerCase().equals(element) && (preceding.getWord().toLowerCase().equals("for") || preceding.getWord().toLowerCase().equals("of") || preceding.getWord().toLowerCase().equals("so") || preceding.getWord().toLowerCase().equals("untill"))) {
                return true;
            }
        }
        return false;
    }

    public boolean isCommonNoun(Word preceding, String word) {
        if (preceding == null) {
            return false;
        }
        if (isArticle(preceding.getWord())) {
            return true;
        }
        return false;
    }

    public boolean isProperNoun(Word preceding, String word) {
        if (Character.isUpperCase(word.charAt(0)) && preceding == null) {
            return true;
        }
        if (Character.isUpperCase(word.charAt(0))) {
            return true;
        }
        return false;
    }

    public boolean isPronoun(Word preceding, String word) {
        if (preceding == null) {
            return true;
        }
        if (isPunctuation(preceding.getWord())) {
            return true;
        }
        if (preceding.getTag().startsWith("VB") || preceding.getTag().startsWith("RB")) {
            return true;
        }
        return false;
    }

    public boolean isPlural(String word) {
        if (word.endsWith("s")) {
            return true;
        }
        return false;
    }

    public boolean isVerb(Word preceding, String word) {
        if (preceding == null) {
            return false;
        }
        if (preceding.getTag().equals("PRP")) {
            return true;
        }
        return false;
    }

    public boolean isPastVerb(Word preceding, String word) {
        if (preceding == null) {
            return false;
        }
        if (word.length() > 2) {
            if (word.substring(word.length() - 2).equals("ed")) {
                return true;
            }
        }
        return false;
    }

    public boolean isGerund(Word preceding, String word) {
        if (word.length() > 3) {
            if (word.substring(word.length() - 3).equals("ing")) {
                return true;
            }
        }
        return false;
    }

    public boolean isPastParticiple(Word preceding, String word) {
        if (preceding == null) {
            return false;
        }
        if (word.length() > 2) {
            if (word.substring(word.length() - 2).equals("en")) {
                return true;
            }
        }
        return false;
    }

    public boolean isVBP(Word preceding, String word) {
        if (preceding == null) {
            return false;
        }
        if (preceding.getTag().equals("PRP")) {
            return true;
        }
        return false;
    }

    public boolean isVBZ(Word preceding, String word) {
        if (preceding == null) {
            return false;
        }
        if (word.length() > 1) {
            if (word.substring(word.length() - 1).equals("s")) {
                return true;
            }
        }
        return false;
    }

    public boolean isModalVerb(Word preceding, String word) {
        if (preceding == null) {
            return false;
        }
        if (preceding.getTag().equals("PRP")) {
            return true;
        }
        return false;
    }

    public boolean isTo(String word) {
        if (word.equals("to")) {
            return true;
        }
        return false;
    }

    public boolean isAdjective(Word preceding, String word) {
        if (preceding == null) {
            return false;
        }
        if (preceding.getWord().equals("is") || preceding.getWord().equals("are") || preceding.getWord().equals("been")) {
            return true;
        }
        return false;
    }

    public boolean isAdverb(Word preceding, String word) {
        if (preceding != null && (preceding.getTag().startsWith("VB") || preceding.getTag().startsWith("NN"))) {
            return true;
        }
        if (word.endsWith("ly")) {
            return true;
        }
        return false;
    }

    public boolean isComparative(String word) {
        if (word.endsWith("er")) {
            return true;
        }
        return false;
    }

    public boolean isSuperlative(String word) {
        if (word.endsWith("est")) {
            return true;
        }
        return false;
    }

    public boolean isParticiple(Word preceding, String word) {
        if (preceding == null) {
            return false;
        }
        if (preceding.getTag().startsWith("VB")) {
            return true;
        }
        return false;
    }

    public boolean isNumber(String word) {
        int count = 0;
        for (int i = 0; i < word.length(); i++) {
            if (Character.isDigit(word.charAt(i))) {
                count++;
            }
        }
        if (count == word.length()) {
            return true;
        }
        return false;
    }

    public boolean isPunctuation(String word) {
        if (word.length() > 1) {
            return false;
        }
        if (word.trim().equals(".")) {
            return true;
        }
        if (word.trim().equals(",")) {
            return true;
        }
        if (word.trim().equals("\"")) {
            return true;
        }
        if (word.trim().equals("'")) {
            return true;
        }
        if (word.trim().equals("*")) {
            return true;
        }
        if (word.trim().equals("$")) {
            return true;
        }
        return false;
    }

    public boolean wordIsValid(String word) {
        if(isPunctuation(word)) return true;
        if (Character.isDigit(word.charAt(0))) {
            for (int i = 0; i < word.length(); i++) {
                if (!Character.isDigit(word.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
        for (int i = 0; i < word.length(); i++) {
            if (!Character.isAlphabetic(word.charAt(i)) && word.charAt(i) != '.' && word.charAt(i) != '-') {
                return false;
            }
        }
        return true;
    }

    public void printAll() {
        Iterator<Map.Entry<Integer, WordTagPair>> tag = tagSequence.entrySet().iterator();
        while (tag.hasNext()) {
            Map.Entry<Integer, WordTagPair> tagEntry = tag.next();
            System.out.println(tagEntry.getValue().toString());
        }
    }

    public void test() throws IOException {
        makeSequence();
        printAll();
    }

}
