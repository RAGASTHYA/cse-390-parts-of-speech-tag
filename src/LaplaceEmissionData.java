/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rahul
 */
public class LaplaceEmissionData {
    double laplacePossibility;
    
    String word;
    String tag;

    public LaplaceEmissionData(double laplacePossibility, String word, String tag) {
        this.laplacePossibility = laplacePossibility;
        this.word = word;
        this.tag = tag;
    }

    public double getLaplacePossibility() {
        return laplacePossibility;
    }

    public void setLaplacePossibility(double laplacePossibility) {
        this.laplacePossibility = laplacePossibility;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    
    public String toString() {
        return String.format("%-20s%-20s%-20s", word, tag, laplacePossibility);
    }
}
