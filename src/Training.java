
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rahul
 */
public class Training {

    FileWriter mleEmissions;
    FileWriter laplaceEmissions;

    FileWriter mleTransitions;
    FileWriter laplaceTransitions;

    FileWriter laplaceTags;

    ArrayList<Sentence> data;

    Hashtable<Double, Tags> tagset;
    Hashtable<Double, String> wordset;

    ArrayList<String> tagList, wordList;

    Hashtable<Double, MLETransitionData> mleTransition;
    Hashtable<Double, MLEEmissionData> mleEmission;
    Hashtable<Double, LaplaceTransitionData> laplaceTransition;
    Hashtable<Double, LaplaceEmissionData> laplaceEmission;

    public Training(String trainingData) throws FileNotFoundException, IOException {
        TextData training = new TextData();
        training.readTraining(trainingData);

        data = training.getTrainingData();

        mleEmissions = new FileWriter("MLE-Emissions.txt");
        laplaceEmissions = new FileWriter("Laplace-Emissions.txt");
        mleTransitions = new FileWriter("MLE-Transitions.txt");
        laplaceTransitions = new FileWriter("Laplace-Transitions.txt");
        laplaceTags = new FileWriter("Laplace-Tags.txt");

        tagset = new Hashtable<Double, Tags>();
        wordset = new Hashtable<Double, String>();

        tagList = new ArrayList<String>();
        wordList = new ArrayList<String>();

        mleTransition = new Hashtable<Double, MLETransitionData>();
        mleEmission = new Hashtable<Double, MLEEmissionData>();
        laplaceTransition = new Hashtable<Double, LaplaceTransitionData>();
        laplaceEmission = new Hashtable<Double, LaplaceEmissionData>();

    }

    public int countWords(String word) {
        int count = 0;
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Word> words = data.get(i).getWords();
            for (int j = 0; j < words.size() - 1; j++) {
                Word pair = words.get(j);

                if (pair.word.trim().equals(word)) {
                    count++;
                }
            }
        }
        return count;
    }

    public void getWordData() {
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Word> words = data.get(i).getWords();
            for (int j = 0; j < words.size(); j++) {
                String word = words.get(j).word;
                int count = countWords(word);
                int total = trainingSize();
                double possibility = (double) count / (double) total;
                if (wordset.get(possibility) != null) {
                    continue;
                } else {
                    wordset.put(possibility, word);
                }
            }
        }
    }

    public int countTagPairs(String tag1, String tag2) {
        int count = 0;
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Word> words = data.get(i).getWords();
            for (int j = 0; j < words.size() - 1; j++) {
                Word pair1 = words.get(j);
                Word pair2 = words.get(j + 1);

                if ((pair1.tag.trim().equals(tag1)) && (pair2.tag.trim().equals(tag2))) {
                    count++;
                }
            }
        }

        return count;
    }

    public int countWords(String tag, String word) {
        int count = 0;
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Word> words = data.get(i).getWords();
            for (int j = 0; j < words.size(); j++) {

                if ((words.get(j).word.trim().equals(word)) && (words.get(j).tag.trim().equals(tag))) {
                    count++;
                }
            }
        }

        return count;
    }

    public int countTags(String tag) {
        int count = 0;
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Word> words = data.get(i).getWords();
            for (int j = 0; j < words.size(); j++) {

                if (words.get(j).tag.equals(tag)) {
                    count++;
                }
            }
        }

        return count;
    }

    public int trainingSize() {
        int count = 0;
        for (int i = 0; i < data.size(); i++) {
            count += data.get(i).getWords().size();
        }
        return count;
    }

    public double getTagPossibility(String tag) {
        return (double) countTags(tag) / (double) trainingSize();
    }

    public void getTagData() {
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Word> words = data.get(i).getWords();
            for (int j = 0; j < words.size(); j++) {
                Tags tag = new Tags(words.get(j).tag, getTagPossibility(words.get(j).tag));
                if (tagset.get(tag.possibility) != null) {
                    continue;
                } else {
                    tagset.put(tag.possibility, tag);
                }
            }
        }
    }

    public void makeDataLists() {
        getWordData();
        getTagData();
        Iterator<Map.Entry<Double, Tags>> itTags = tagset.entrySet().iterator();
        while (itTags.hasNext()) {
            Map.Entry<Double, Tags> tagElement = itTags.next();
            tagList.add(tagElement.getValue().tag);
        }

        Iterator<Map.Entry<Double, String>> itWords = wordset.entrySet().iterator();
        while (itWords.hasNext()) {
            Map.Entry<Double, String> tagElement = itWords.next();
            wordList.add(tagElement.getValue());
        }
    }

    public void getTransitions() {
        int total = trainingSize();
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Word> words = data.get(i).getWords();
            for (int j = 0; j < words.size() - 1; j++) {
                int counttagpairs = countTagPairs(words.get(j).tag, words.get(j + 1).tag);
                int counttags = countTags(words.get(j).tag);
                if (counttags > 0) {
                    double mle = (double) counttagpairs / counttags;
                    double laplace = (double) (counttagpairs + 1) / (counttags + total);
                    MLETransitionData newMLEData = new MLETransitionData(mle, words.get(j).tag, words.get(j + 1).tag);
                    if (mleTransition.get(newMLEData.mlePossibility) != null) {
                        continue;
                    } else {
                        mleTransition.put(newMLEData.mlePossibility, newMLEData);
                    }
                    LaplaceTransitionData newLaplaceData = new LaplaceTransitionData(laplace, words.get(j).tag, words.get(j + 1).tag);
                    if (laplaceTransition.get(newLaplaceData.laplacePossibility) != null) {
                        continue;
                    } else {
                        laplaceTransition.put(newLaplaceData.laplacePossibility, newLaplaceData);
                    }
                } else {
                    continue;
                }
            }
        }
    }

    public void getEmissions() {
        int total = trainingSize();
        for (int i = 0; i < data.size(); i++) {
            ArrayList<Word> words = data.get(i).getWords();
            for (int j = 0; j < words.size() - 1; j++) {
                int countwords = countWords(words.get(j).tag, words.get(j).word);
                int counttags = countTags(words.get(j).tag);
                if (counttags > 0) {
                    double mle = (double) countwords / counttags;
                    double laplace = (double) (countwords + 1) / (double) (counttags + total);
                    MLEEmissionData newMLEData = new MLEEmissionData(mle, words.get(j).word, words.get(j + 1).tag);
                    if (mleEmission.get(newMLEData.mlePossibility) != null) {
                        continue;
                    } else {
                        mleEmission.put(newMLEData.mlePossibility, newMLEData);
                    }
                    LaplaceEmissionData newLaplaceData = new LaplaceEmissionData(laplace, words.get(j).word, words.get(j + 1).tag);
                    if (laplaceEmission.get(newLaplaceData.laplacePossibility) != null) {
                        continue;
                    } else {
                        laplaceEmission.put(newLaplaceData.laplacePossibility, newLaplaceData);
                    }
                } else {
                    continue;
                }
            }
        }
    }

    public void writeFiles() throws IOException {
        BufferedWriter mleEWriter = new BufferedWriter(mleEmissions);
        BufferedWriter laplaceEWriter = new BufferedWriter(laplaceEmissions);
        BufferedWriter mleTWriter = new BufferedWriter(mleTransitions);
        BufferedWriter laplaceTWriter = new BufferedWriter(laplaceTransitions);
        BufferedWriter tagsEWriter = new BufferedWriter(laplaceTags);

        getTransitions();
        getEmissions();

        Iterator<Map.Entry<Double, MLETransitionData>> itMLET = mleTransition.entrySet().iterator();
        while (itMLET.hasNext()) {
            Map.Entry<Double, MLETransitionData> mleT = itMLET.next();
            mleTWriter.write(mleT.getValue().toString());
            mleTWriter.write("\n");
        }
        mleTWriter.close();

        Iterator<Map.Entry<Double, LaplaceTransitionData>> itLPT = laplaceTransition.entrySet().iterator();
        while (itLPT.hasNext()) {
            Map.Entry<Double, LaplaceTransitionData> lpT = itLPT.next();
            laplaceTWriter.write(lpT.getValue().toString());
            laplaceTWriter.write("\n");
        }
        laplaceTWriter.close();

        Iterator<Map.Entry<Double, MLEEmissionData>> itMLEE = mleEmission.entrySet().iterator();
        while (itMLEE.hasNext()) {
            Map.Entry<Double, MLEEmissionData> mleE = itMLEE.next();
            mleEWriter.write(mleE.getValue().toString());
            mleEWriter.write("\n");
        }
        mleEWriter.close();

        Iterator<Map.Entry<Double, LaplaceEmissionData>> itLPE = laplaceEmission.entrySet().iterator();
        while (itLPE.hasNext()) {
            Map.Entry<Double, LaplaceEmissionData> lpE = itLPE.next();
            laplaceEWriter.write(lpE.getValue().toString());
            laplaceEWriter.write("\n");
        }
        laplaceEWriter.close();

        getTagData();

        Iterator<Map.Entry<Double, Tags>> itTags = tagset.entrySet().iterator();
        while (itTags.hasNext()) {
            Map.Entry<Double, Tags> tagElement = itTags.next();
            tagsEWriter.write(tagElement.getValue().toString());
            tagsEWriter.write("\n");
        }
        tagsEWriter.close();
    }

    public ArrayList<ArrayList<Double>> TransitionMatrix() {
        makeDataLists();
        ArrayList<ArrayList<Double>> matrixA = new ArrayList<ArrayList<Double>>();

        for (int i = 0; i < tagList.size(); i++) {
            ArrayList<Double> list = new ArrayList<Double>();
            for (int j = 0; j < tagList.size(); j++) {
                int counttagpairs = countTagPairs(tagList.get(i), tagList.get(j));
                int counttags = countTags(tagList.get(j));
                double possibility = 0;
                if (counttags > 0) {
                    possibility = (double) counttagpairs / counttags;
                }
                list.add(possibility);
            }
            matrixA.add(list);
        }

        return matrixA;
    }

    public ArrayList<ArrayList<Double>> EmissionMatrix() {
        makeDataLists();
        ArrayList<ArrayList<Double>> matrixB = new ArrayList<ArrayList<Double>>();

        for (int i = 0; i < tagList.size(); i++) {
            ArrayList<Double> list = new ArrayList<Double>();
            for (int j = 0; j < wordList.size(); j++) {
                int countwords = countWords(tagList.get(j), wordList.get(j));
                int counttags = countTags(tagList.get(j));
                double possibility = 0;
                if (counttags > 0) {
                    possibility = (double) countwords / counttags;
                }
                list.add(possibility);
            }
            matrixB.add(list);
        }

        return matrixB;
    }

    public FileWriter getMleTransitions() {
        return mleTransitions;
    }

    public Hashtable<Double, Tags> getTagset() {
        return tagset;
    }

    public Hashtable<Double, String> getWordset() {
        return wordset;
    }

    public ArrayList<String> getWordList() {
        return wordList;
    }

    public Hashtable<Double, MLETransitionData> getMleTransition() {
        return mleTransition;
    }

    public Hashtable<Double, MLEEmissionData> getMleEmission() {
        return mleEmission;
    }

    public Hashtable<Double, LaplaceTransitionData> getLaplaceTransition() {
        return laplaceTransition;
    }

    public Hashtable<Double, LaplaceEmissionData> getLaplaceEmission() {
        return laplaceEmission;
    }

    public ArrayList<String> getTagList() {
        return tagList;
    }

    public void setTagList(ArrayList<String> tagList) {
        this.tagList = tagList;
    }
    
    
}
