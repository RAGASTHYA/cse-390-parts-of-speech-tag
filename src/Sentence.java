/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;

/**
 *
 * @author Rahul S Agasthya
 * sbuid 109961109
 */
public class Sentence {
    int number;
    ArrayList<Word> words;
    
    public Sentence() {}

    public Sentence(int number, ArrayList<Word> words) {
        this.number = number;
        this.words = words;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ArrayList<Word> getWords() {
        return words;
    }

    public void setWords(ArrayList<Word> words) {
        this.words = words;
    }
    
    public String toString() {
        String s = String.valueOf(number) + "\t";
        for(int i=0; i<words.size(); i++)
            s = s + words.get(i).toString() + " ";
        s = s + "\n";
        return s;
    }
}
