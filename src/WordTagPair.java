/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rahul
 */
public class WordTagPair {
    String word;
    String tag;
    
    boolean predicted;

    public WordTagPair(String word, String tag,  boolean predicted) {
        this.word = word;
        this.tag = tag;
        this.predicted = predicted;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isPredicted() {
        return predicted;
    }

    public void setPredicted(boolean predicted) {
        this.predicted = predicted;
    }
    
    public String toString() {
        return String.format("%-20s%-20s%-10s", tag, word, predicted);
    }
}
