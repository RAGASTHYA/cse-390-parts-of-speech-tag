/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Rahul S Agasthya
 * sbuid 109961109
 */
public class TextData {
    ArrayList<Sentence> trainingData;
    ArrayList<Sentence> testData;
    
    public TextData() {
        trainingData = new ArrayList<Sentence>();
        testData = new ArrayList<Sentence>();
    }
    
    public void readTraining(String trainingFile) throws FileNotFoundException {
        Scanner trainingInput = new Scanner(new File(trainingFile));
        while(trainingInput.hasNext()){
            String sentence = trainingInput.nextLine();
            String[] splitSentences = sentence.split("\t");
            int number = Integer.parseInt(splitSentences[0]);
            String words = splitSentences[1];
            String[] wordsArray = words.trim().split(" ");
            ArrayList<Word> sentenceWords = new ArrayList<Word>();
            for(int i=0; i<wordsArray.length; i++) {
                String[] wordTag = wordsArray[i].split("/");
                Word word = new Word(wordTag[0], wordTag[1]);
                sentenceWords.add(word);
            }
            Sentence newSentence = new Sentence(number, sentenceWords);
            trainingData.add(newSentence);
        }
    }
    
    public void readTest(String testFile) throws FileNotFoundException {
        Scanner trainingInput = new Scanner(new File(testFile));
        while(trainingInput.hasNext()){
            String sentence = trainingInput.nextLine();
            String[] splitSentences = sentence.split("\t");
            int number = Integer.parseInt(splitSentences[0]);
            String words = splitSentences[1];
            String[] wordsArray = words.trim().split(" ");
            ArrayList<Word> sentenceWords = new ArrayList<Word>();
            for(int i=0; i<wordsArray.length; i++) {
                String[] wordTag = wordsArray[i].split("/");
                if(wordTag.length<2)continue;
                Word word = new Word(wordTag[0], wordTag[1]);
                sentenceWords.add(word);
            }
            Sentence newSentence = new Sentence(number, sentenceWords);
            testData.add(newSentence);
        }
    }
    
    public void printSentences() {
        System.out.println("----------------------TRAINING DATA----------------------");
        for(int i=0; i<trainingData.size(); i++)
            System.out.print(trainingData.get(i).toString());
    }
    
    public void printTestSentences() {
        System.out.println("----------------------TEST DATA GENERATED----------------------");
        for(int i=0; i<testData.size(); i++)
            System.out.print(testData.get(i).toString());
    }

    public ArrayList<Sentence> getTrainingData() {
        return trainingData;
    }

    public ArrayList<Sentence> getTestData() {
        return testData;
    }
}
