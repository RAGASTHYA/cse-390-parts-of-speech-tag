/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Rahul S Agasthya
 * sbuid 109961109
 */
public class Word {
    String word;
    String tag;

    public Word() {
        
    }
    
    public Word(String word, String tag) {
        this.word = word;
        this.tag = tag;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    
    public String toString() {
        return String.format("%-15s, %-15s;", word, tag);
    }
}
