/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rahul
 */
public class MLEEmissionData {
    double mlePossibility;
    
    String word;
    String tag;

    public MLEEmissionData(double mlePossibility, String word, String tag) {
        this.mlePossibility = mlePossibility;
        this.word = word;
        this.tag = tag;
    }

    public double getMlePossibility() {
        return mlePossibility;
    }

    public void setMlePossibility(double mlePossibility) {
        this.mlePossibility = mlePossibility;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    
    public String toString() {
        return String.format("%-20s%-20s%-20s", word, tag, mlePossibility);
    }
}
