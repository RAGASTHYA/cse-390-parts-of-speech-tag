
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rahul
 */
public class Evaluator {

    BaselineTagger tagger;
    Hashtable<Integer, WordTagPair> tagSequence;
    TextData taggedData;

    public Evaluator(BaselineTagger initTagger, Hashtable<Integer, WordTagPair> initTagSequence) throws FileNotFoundException {
        tagger = initTagger;
        tagSequence = initTagSequence;
        taggedData = new TextData();
        taggedData.readTest("Test-Generated.txt");
    }

    public double getAccurary() {
        int correctCount = 0;
        int countPredicted = 0;

        Iterator<Map.Entry<Integer, WordTagPair>> tag = tagSequence.entrySet().iterator();
        while (tag.hasNext()) {
            Map.Entry<Integer, WordTagPair> tagEntry = tag.next();
            if (tagEntry.getValue().isPredicted()) {
                countPredicted++;
            }
        }
        int i = 0, j = 0;
        while (i < taggedData.testData.size() && j < tagger.test.testData.size()) {
            ArrayList<Sentence> generated = taggedData.testData;
            ArrayList<Sentence> actual = tagger.test.testData;

            int k = 0, l = 0;
            while (k < generated.get(i).getWords().size() && l < actual.get(j).getWords().size()) {
                Word genWord = generated.get(i).getWords().get(k);
                Word actWord = actual.get(i).getWords().get(l);

                if (genWord.getTag().equals(actWord.getTag()) && genWord.getWord().equals(actWord.getWord())) {
                    correctCount++;
                }

                k++;
                l++;
            }
            i++;
            j++;
        }

        return (double) correctCount / (double) countPredicted;
    }

    public void printPrecision() {
        int correctPredict = 0;
        int countPredicted = 0;

        System.out.println("PRECISION FOR TEST TAGS");
        Iterator<Map.Entry<Integer, WordTagPair>> tag = tagSequence.entrySet().iterator();
        while (tag.hasNext()) {
            Map.Entry<Integer, WordTagPair> tagEntry = tag.next();
            if (tagEntry.getValue().isPredicted()) {
                countPredicted++;

                int i = 0, j = 0;
                while (i < taggedData.testData.size() && j < tagger.test.testData.size()) {
                    ArrayList<Sentence> generated = taggedData.testData;
                    ArrayList<Sentence> actual = tagger.test.testData;

                    int k = 0, l = 0;
                    while (k < generated.get(i).getWords().size() && l < actual.get(j).getWords().size()) {
                        Word genWord = generated.get(i).getWords().get(k);
                        Word actWord = actual.get(i).getWords().get(l);

                        if (genWord.getTag().equals(actWord.getTag()) && genWord.getWord().equals(actWord.getWord()) && tagEntry.getValue().getTag().equals(actWord.getTag())) {
                            correctPredict++;
                        }

                        k++;
                        l++;
                    }
                    i++;
                    j++;
                }

                double precision = (double) correctPredict / (double) countPredicted;

                System.out.printf("%-30s%-20s\n", tagEntry.getValue().tag, precision);
            }
        }
    }

    public void printRecall() {
        int correctPredict = 0;
        int countTag = 0;

        System.out.println("RECALLS FOR TEST TAGS");
        Iterator<Map.Entry<Integer, WordTagPair>> tag = tagSequence.entrySet().iterator();
        while (tag.hasNext()) {
            Map.Entry<Integer, WordTagPair> tagEntry = tag.next();
            if (tagEntry.getValue().isPredicted()) {
                int i = 0, j = 0;
                while (i < taggedData.testData.size() && j < tagger.test.testData.size()) {
                    ArrayList<Sentence> generated = taggedData.testData;
                    ArrayList<Sentence> actual = tagger.test.testData;

                    int k = 0, l = 0;
                    while (k < generated.get(i).getWords().size() && l < actual.get(j).getWords().size()) {
                        Word genWord = generated.get(i).getWords().get(k);
                        Word actWord = actual.get(i).getWords().get(l);

                        if (genWord.getTag().equals(actWord.getTag()) && genWord.getWord().equals(actWord.getWord()) && tagEntry.getValue().getTag().equals(actWord.getTag())) {
                            correctPredict++;
                        }

                        k++;
                        l++;
                    }
                    i++;
                    j++;
                }
            }

            countTag = tagEntry.getKey();

            double recall = (double) correctPredict / (double) countTag;

            System.out.printf("%-30s%-20s\n", tagEntry.getValue().tag, recall);
        }
    }

    public void printF1() {
        int correctPredict = 0;
        int countPredicted = 0;
        int countTag = 0;

        System.out.println("F1 FOR TEST TAGS");
        Iterator<Map.Entry<Integer, WordTagPair>> tag = tagSequence.entrySet().iterator();
        while (tag.hasNext()) {
            Map.Entry<Integer, WordTagPair> tagEntry = tag.next();
            if (tagEntry.getValue().isPredicted()) {
                countPredicted++;

                int i = 0, j = 0;
                while (i < taggedData.testData.size() && j < tagger.test.testData.size()) {
                    ArrayList<Sentence> generated = taggedData.testData;
                    ArrayList<Sentence> actual = tagger.test.testData;

                    int k = 0, l = 0;
                    while (k < generated.get(i).getWords().size() && l < actual.get(j).getWords().size()) {
                        Word genWord = generated.get(i).getWords().get(k);
                        Word actWord = actual.get(i).getWords().get(l);

                        if (genWord.getTag().equals(actWord.getTag()) && genWord.getWord().equals(actWord.getWord()) && tagEntry.getValue().getTag().equals(actWord.getTag())) {
                            correctPredict++;
                        }

                        k++;
                        l++;
                    }
                    i++;
                    j++;
                }
                countTag = tagEntry.getKey();

                double precision = (double) correctPredict / (double) countPredicted;
                double recall = (double) correctPredict / (double) countTag;
                
                double f1 = (2*precision*recall)/(precision+recall);

                System.out.printf("%-30s%-20s\n", tagEntry.getValue().tag, f1);
            }
        }
    }
}
