Copy and Paste the test.txt and training.txt files in ./src folder of the project.
Ensure the name of the training file is "training.txt" and that of the test file is "test.txt".
Run the Driver.java class.

Be patient, the program will take at least 8 minutes to run to completion.
